var patrat = document.querySelectorAll(".patrat");

var counter = 0;

var matrice = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
];

function reset_matrix(matrice) {
    let count = 1;
    for (let i = 0; i < 3; i++)
        for (let j = 0; j < 3; j++)
            matrice[i][j] = count++;
}

reset_matrix(matrice);

function validare_coloane(matrice) {
    for (let j = 0; j < 3; j++) {
        let litera = matrice[0][j];
        let ok = 1;
        coloana = j;
        for (let i = 1; i < 3; i++) {
            if (matrice[i][j] != litera) {
                ok = 0;
                break;
            }
        }
        if (ok == 1) {
            console.log(rand);
            Array.from(patrat).forEach((element) => {
                    if(element.id == coloana + 1 || element.id == coloana + 4 || element.id == coloana + 7)
                        element.style.backgroundColor = "#12664F";
            })
            return litera;
        }
    }
    return 0;
}

function validare_linii(matrice) {
    for (let i = 0; i < 3; i++) {
        let litera = matrice[i][0];
        let ok = 1;
        rand = i;
        for (let j = 1; j < 3; j++) {
            if (matrice[i][j] != litera) {
                ok = 0;
                break;
            }
        }
        if (ok == 1){
            console.log(rand);
            Array.from(patrat).forEach((element) => {
                    if(element.id == 3*rand + 1 || element.id == 3*rand + 2 || element.id == 3*rand + 3)
                        element.style.backgroundColor = "#12664F";
            })
            return litera;
        }
    }
    return 0;
}

function validare_diag(matrice) {
    if (matrice[0][0] == matrice[1][1] && matrice[1][1] == matrice[2][2])
    {
        Array.from(patrat).forEach((element) => {
            if(element.id == 1 || element.id == 5 || element.id == 9)
                element.style.backgroundColor = "#12664F";
        })
        return matrice[0][0];
    }
    if (matrice[0][2] == matrice[1][1] && matrice[1][1] == matrice[2][0])
    {
        Array.from(patrat).forEach((element) => {
            if(element.id == 3 || element.id == 5 || element.id == 7)
                element.style.backgroundColor = "#12664F";
        })
        return matrice[1][1];
    }
    return 0;
}

function validare_joc(matrice) {
    let coloane = validare_coloane(matrice);
    if (coloane != 0)
        return coloane;
    let linii = validare_linii(matrice);
    if (linii != 0)
        return linii;
    let diag = validare_diag(matrice);
    if (diag != 0)
        return diag;
    return 0;
}

Array.from(patrat).forEach((element) => {
    element.addEventListener('click', () => {
        if (counter < 9) {
            if (element.innerHTML == "<h1>X</h1>" || element.innerHTML == "<h1>O</h1>") {
                alert("Alege alt patratel!");
            }
            else {
                element.style.backgroundColor = "#484c60"
                let b = element.id - 1;
                if (counter % 2 === 0) {
                    element.innerHTML = "<h1>X</h1>";
                    matrice[Math.floor(b / 3)][b % 3] = 'X';
                }
                else {
                    element.innerHTML = "<h1>O</h1>";
                    matrice[Math.floor(b / 3)][b % 3] = 'O';
                }
                counter++;
            }
        }
        let stare_joc = validare_joc(matrice);
        if(counter == 9 && stare_joc == 0)
            alert("Egalitate!");
        if (stare_joc != 0) {
            counter = 9;
            alert("Jucatorul cu litera " + stare_joc + " a castigat!");
        }
    })
})

// Array.from(patrat).forEach((element) => {
//     element.addEventListener('mouseover', () => {
//         if(element.innerHTML == "<h1>X</h1>" || element.innerHTML == "<h1>O</h1>"){
//             element.style.backgroundColor = "red";
//         }
//     })
//         if(element.innerHTML == "<h1>X</h1>" || element.innerHTML == "<h1>O</h1>"){
//         element.addEventListener('mouseout', () => {
//             element.style.backgroundColor = "#484c60";
//         })}
//         else {
//             element.style.backgroundColor = "#12664F";
//             element.addEventListener('mouseout', () => {
//                 element.style.backgroundColor = "#7A82AB";
//             })
//         }
//     })
// })

// Array.from(patrat).forEach((element) => {
//     console.log("1");
//     if(element.innerHTML == "<h1>X</h1>" || element.innerHTML == "<h1>O</h1>")
//         element.addEventListener('mouseover', () => {
//             element.style.backgroundColor = "red";
//         })
// })


var buton = document.getElementById("buton");

buton.addEventListener('click', () => {
    reset_matrix(matrice);
    counter = 0;
    Array.from(patrat).forEach((element) => {
        element.innerHTML = "";
        element.style.backgroundColor = "#7A82AB";
    })
})


